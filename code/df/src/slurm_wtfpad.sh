#!/bin/bash
#SBATCH --job-name=pickle_duhe_out
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --time=10-00:00:00
                                                           
#SBATCH -o /home/paul.duhe/thesis/new/small_thesis_project/runs/closedworld_wtfpad_run.%j.out
#SBATCH --partition=beards
#SBATCH --mem=128000
#SBATCH --gres=gpu:1
#SBATCH --mail-type=END              
#SBATCH --mail-user=paul.duhe@nps.edu
 
#. /etc/profile
#/share/spack/gcc-7.2.0/miniconda3-4.5.12-gkh/condabin/conda activate tf-2.6
 
python3 /home/paul.duhe/thesis/new/small_thesis_project/code/df/src/ClosedWorld_DF_WTFPAD.py
